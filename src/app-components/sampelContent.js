var sampleContent = {
	"samples": [
		{
			"sectionTitle" : "List 1",
			"sectionID"    : "list1",
			"sectionLinks" : [
				{
					"id"   : "1l1",
					"name" : "1 Item 1",
					"url"  : "#item1",
					"fav"  : true
				},
				{
					"id"   : "1l2",
					"name" : "1 Item 2",
					"url"  : "#item2",
					"fav"  : false
				},
				{
					"id"   : "1l3",
					"name" : "1 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "1l4",
					"name" : "1 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "1l5",
					"name" : "1 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		},
		{
			"sectionTitle" : "List 2",
			"sectionID"    : "list2",
			"sectionLinks" : [
				{
					"id"   : "2l1",
					"name" : "2 Item 1",
					"url"  : "#item1",
					"fav"  : false
				},
				{
					"id"   : "2l2",
					"name" : "2 Item 2",
					"url"  : "#item2",
					"fav"  : true
				},
				{
					"id"   : "2l3",
					"name" : "2 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "2l4",
					"name" : "2 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "2l5",
					"name" : "2 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		},
		{
			"sectionTitle" : "List 3",
			"sectionID"    : "list3",
			"sectionLinks" : [
				{
					"id"   : "3l1",
					"name" : "3 Item 1",
					"url"  : "#item1",
					"fav"  : false
				},
				{
					"id"   : "3l2",
					"name" : "3 Item 2",
					"url"  : "#item2",
					"fav"  : false
				},
				{
					"id"   : "3l3",
					"name" : "3 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "3l4",
					"name" : "3 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "3l5",
					"name" : "3 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		},
		{
			"sectionTitle" : "List 4",
			"sectionID"    : "list4",
			"sectionLinks" : [
				{
					"id"   : "4l1",
					"name" : "4 Item 1",
					"url"  : "#item1",
					"fav"  : false
				},
				{
					"id"   : "4l2",
					"name" : "4 Item 2",
					"url"  : "#item2",
					"fav"  : false
				},
				{
					"id"   : "4l3",
					"name" : "4 Item 3",
					"url"  : "#item3",
					"fav"  : false
				},
				{
					"id"   : "4l4",
					"name" : "4 Item 4",
					"url"  : "#item4",
					"fav"  : false
				},
				{
					"id"   : "4l5",
					"name" : "4 Item 5",
					"url"  : "#item5",
					"fav"  : false
				},
			]
		}
	]
}

var favoritesReports = {
	"favorites": [
		// {
		// 	"id"   : "fav1",
		// 	"name" : "Engineer Best-of-breed",
		// 	"url"  : "#fav1"
		// },
		// {
		// 	"id"   : "fav2",
		// 	"name" : "Rss-capable Best-of-breed",
		// 	"url"  : "#fav2"
		// },
		// {
		// 	"id"   : "fav3",
		// 	"name" : "Repurpose Turn-key",
		// 	"url"  : "#fav3"
		// },
		// {
		// 	"id"   : "fav4",
		// 	"name" : "Reinvent Streamline",
		// 	"url"  : "#fav4"
		// },
		// {
		// 	"id"   : "fav5",
		// 	"name" : "Customized E-commerce",
		// 	"url"  : "#fav5"
		// },
		// {
		// 	"id"   : "fav6",
		// 	"name" : "Open-source Tagclouds",
		// 	"url"  : "#fav6"
		// },
		// {
		// 	"id"   : "fav7",
		// 	"name" : "Long-tail Bricks-and-clicks",
		// 	"url"  : "#fav7"
		// },
		// {
		// 	"id"   : "fav8",
		// 	"name" : "User-centred Web-readiness",
		// 	"url"  : "#fav8"
		// },
		// {
		// 	"id"   : "fav9",
		// 	"name" : "E-markets Efficient",
		// 	"url"  : "#fav9"
		// },
		// {
		// 	"id"   : "fav10",
		// 	"name" : "Supply-chains Enable",
		// 	"url"  : "#fav10"
		// }
	]
}

var savedReports = {
	"reports": [
		{
			"id"   : "rep1",
			"name" : "Report 1",
			"url"  : "#rep1"
		},
		{
			"id"   : "rep2",
			"name" : "Report 2",
			"url"  : "#rep2"
		},
		{
			"id"   : "rep3",
			"name" : "Report 3",
			"url"  : "#rep3"
		},
		{
			"id"   : "rep4",
			"name" : "Report 4",
			"url"  : "#rep4"
		},
		{
			"id"   : "rep5",
			"name" : "Report 5",
			"url"  : "#rep5"
		},
		{
			"id"   : "rep6",
			"name" : "Report 6",
			"url"  : "#rep6"
		},
		{
			"id"   : "rep7",
			"name" : "Report 7",
			"url"  : "#rep7"
		},
		{
			"id"   : "rep8",
			"name" : "Report 8",
			"url"  : "#rep8"
		},
		{
			"id"   : "rep9",
			"name" : "Report 9",
			"url"  : "#rep9"
		},
		{
			"id"   : "rep10",
			"name" : "Report 10",
			"url"  : "#rep10"
		}
	]
}
