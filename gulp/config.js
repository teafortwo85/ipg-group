var args = require('minimist')(process.argv.slice(2));
var VERSION = args.version || require('../package.json').version;

module.exports = {
	banner:
	'/*!\n' +
	' * IPS Group\n' +
	' * v' + VERSION + '\n' +
	' */\n',
	projectName: 'ipsgroup',
	scssBase: [
		'src/utils/**/*.scss',               // All Helpers
		'src/variables/variables.scss',      // Base variables
		'src/variables/color-base.json',     // Base colors
		'src/variables/color-palette.json'   // Base color palettes
	],
	themeList: [
		'default',
		'default-reverse'
	],
	paths: 'src/app-components/**',
	themePath: 'src/variables/themes/**',
	outputDir: 'dist/'
};