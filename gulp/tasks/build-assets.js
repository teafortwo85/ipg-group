
// File Imports
var config    = require('../config');

// NPM Imports
var gulp         = require('gulp');
var gutil        = require('gulp-util');
var series       = require('stream-series');

// gulp build-assets
exports.task = function() {
	var streams    = [];

	var dest       = config.outputDir,
			assetPipe  = undefined;

	gutil.log("Building assets...");

	// fonts
	streams.push(
		assetPipe = gulp.src('src/fonts/**/*')
			.pipe(gulp.dest(dest + '/fonts'))
	);

	// icons
	streams.push(
		assetPipe = gulp.src('src/icons/**/*')
			.pipe(gulp.dest(dest + '/icons'))
	);

	// images
	streams.push(
		assetPipe = gulp.src('src/images/**/*')
			.pipe(gulp.dest(dest + '/images'))
	);

	// 3rd party css
	streams.push(
		assetPipe = gulp.src('src/3rd-party/**/*.css')
			.pipe(gulp.dest(dest + '/css'))
	);

	// 3rd party js
	streams.push(
		assetPipe = gulp.src('src/3rd-party/**/*.js')
			.pipe(gulp.dest(dest + '/js'))
	);

	return series(streams);
};
