
// File Imports
var config    = require('../config');
var util      = require('../util');

// NPM Imports
var autoprefixer = require('gulp-autoprefixer');
var gulp         = require('gulp');
var concat       = require('gulp-concat');
var filter       = require('gulp-filter');
var gutil        = require('gulp-util');
var insert       = require('gulp-insert');
var jsonSass     = require('gulp-json-sass');
var minifyCss    = require('gulp-minify-css');
var path         = require('path');
var rename       = require('gulp-rename');
var sass         = require('gulp-sass');
var series       = require('stream-series');

// Local Vars
var args      = util.args;

// gulp build-scss
exports.task = function() {
	var streams    = [];
	var themeList  = config.themeList;

	var dest       = config.outputDir,
			scssPipe   = undefined,
			themePipe  = undefined;

	gutil.log("Building css files...");

	streams.push(
		scssPipe = gulp.src(config.scssBase.concat(
				[path.join(config.paths, '/*-vars.scss'),
				 path.join(config.paths, '/scss/*.scss')]
			))
			.pipe(jsonSass({ sass: false }))
			.pipe(util.filterNonCodeFiles())
			.pipe(filter(['**', '!**/*.css']))
			.pipe(filter(['**', '!**/*-theme.scss']))
			.pipe(filter(['**', '!**/*-layout.scss']))
			.pipe(concat(config.projectName + '-library.scss'))
			.pipe(gulp.dest(dest + '/scss'))
			.pipe(sass())
			.pipe(insert.prepend(config.banner))
			.pipe(concat(config.projectName + '-library.css'))
			.pipe(autoprefixer('last 2 version'))
			.pipe(gulp.dest(dest + '/css'))
			.pipe(minifyCss())
			.pipe(rename({extname: '.min.css'}))
			.pipe(gulp.dest(dest + '/css'))
	);

	themeList.forEach(pushList);

	return series(streams);

	function pushList(element, index, array) {
		streams.push(
			themePipe = gulp.src(config.scssBase.concat(
					[path.join(config.themePath, element + '.json'),
					 path.join(config.paths, '*-theme.json'),
					 path.join(config.paths, '*-theme.scss')]
				))
				.pipe(jsonSass({ sass: false }))
				.pipe(concat('theme-' + element + '.scss'))
				.pipe(gulp.dest(dest + 'scss/themes'))
				.pipe(sass())
				.pipe(insert.prepend(config.banner))
				.pipe(concat('theme-' + element + '.css'))
				.pipe(autoprefixer('last 2 version'))
				.pipe(gulp.dest(dest + '/css'))
				.pipe(minifyCss())
				.pipe(rename({extname: '.min.css'}))
				.pipe(gulp.dest(dest + '/css'))
		);
	}
};
