
// File Imports
var config    = require('../config');
var util      = require('../util');

// NPM Imports
var gulp         = require('gulp');
var concat       = require('gulp-concat');
var filter       = require('gulp-filter');
var gutil        = require('gulp-util');
var insert       = require('gulp-insert');
var jshint       = require('gulp-jshint');
var path         = require('path');
var rename       = require('gulp-rename');
var series       = require('stream-series');
var uglify       = require('gulp-uglify');

// Local Vars
var args      = util.args;

// gulp build-js
exports.task = function() {
	var streams    = [];

	var dest       = config.outputDir,
			jsPipe  = undefined;

	gutil.log("Building js files...");

	// data
	streams.push(
		jsPipe = gulp.src('src/app-components/**/*-data.json')
			.pipe(gulp.dest(dest + '/js/json/'))
	);

	// templates
	streams.push(
		jsPipe = gulp.src('src/app-components/**/*-template.html')
			.pipe(gulp.dest(dest + '/js/templates/'))
	);

	// examples
	streams.push(
		jsPipe = gulp.src('src/app-components/**/*-example.html')
			.pipe(gulp.dest(dest + '/js/examples/'))
	);

	// js
	streams.push(
		jsPipe = gulp.src('src/app-components/**/*.js')
			// .pipe(jshint('.jshintrc'))
			// .pipe(jshint.reporter('default'))
			.pipe(concat(config.projectName + '-library.js'))
			.pipe(gulp.dest(dest + '/js'))
			.pipe(uglify())
			.pipe(rename({extname: '.min.js'}))
			.pipe(gulp.dest(dest + '/js'))
	);

	return series(streams);
};
